//app.js
App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)
	// console.log(12222222222222)
    // 登录
    wx.login({
      success: res => {
			// 发送 res.code 到后台换取 openId, sessionKey, unionId
			console.log(res)
			this.globalData.code = res.code;
		 	this.appLogin(res.code);
      },
	  fail: err=>{
		  console.log(err)
	  }
    })

    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
			  //console.log(res)
              this.globalData.userInfo = res.userInfo

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }
      }
    })
  },
	//获取token
	appLogin(code) {
		let that = this,
			url = this.globalData.host + '/appletLogin',
			obj = {
				code: code
			}
			
		this.request('post', url, obj, function(res){
			//sconsole.log(res)
			if(res.data.code === '200'){
				that.globalData.openid = res.data.result.openid;
				that.globalData.session_key = res.data.result.session_key;
				that.globalData.token = res.data.result.token;
				that.globalData.hunters = res.data.result.bountyHuntersId;
				that.globalData.userId = res.data.result.userId;
				that.globalData.ismember = res.data.result.ismember
				that.globalData.memberState = res.data.result.memberState
			}

		})
	},

	/**
	 * 保存微信昵称和头像
	 */
	saveWXInfo(o) {
		let that = this,
			url = that.globalData.host + '/mobile/user/updateById',
			obj = {
				nickname: o.nickName,
				headImgPath: o.avatarUrl
			}
		that.request('post', url, obj, function (res) {
			if (res.data.code === '200') {

			}
		})
	},

	/**
	* 网络请求
	*/
	request(met, url, obj, fun) {
		let that = this
		// 全局添加token
		if (url.indexOf('/appletLogin') == -1){
			obj.token = this.globalData.token
		}

		// 全局非空处理
		for(let k in obj){
			obj[k] = obj[k] == 'null' ? '' : obj[k] == 'undefined' ? '' : (!obj[k]) ? '' : obj[k]
		}

		// 全局添加城市code判断
		if (this.globalData.cityCode && (!obj.cityCode) ){
			obj.cityCode = this.globalData.cityCode
		}

		// 全局添加上架商品判断
		obj.putaway = 1

		wx.request({
			url: url.indexOf('http') === -1 ? (that.globalData.host + url) : url,
			method: met,
			data: obj,
			header: { 'content-type': 'application/x-www-form-urlencoded'},
			success: (res)=>{
				if(res.data.code === '400'){
					wx.showModal({
						title: '提示',
						content: res.data.msg ,
						success(res) {
						
						}
					})
				}
				fun(res);
			},
			fail: res=>{
				console.error(res)
			}
		})

	},

	//位置授权
	location() {
		let that = this;
		wx.getSetting({
			success(res) {
				if (!res.authSetting['scope.userLocation']) {
					wx.authorize({
						scope: 'scope.userLocation',
						success() {

							//获取位置信息
							wx.getLocation({
								type: 'wgs84',
								success(res) {
									console.log(res)
									that.getCity(res)
								}
							})

						},
						fail(err) {
							console.log('fail')
							that.setData({
								haslocation: false
							})
						}
					})
				} else {
					//获取位置信息
					wx.getLocation({
						type: 'wgs84',
						success(res) {
							console.log(res)
							that.getCity(res)
						}
					})

				}
			}
		})

	},
	//获取位置城市
	getCity(v) {

		let that = this,
			url = that.globalData.host + '/position',
			obj = {
				latitude: v.latitude,
				longitude: v.longitude
			}
		that.request('post', url, obj, function (res) {

			if (res.data.code === '200') {
				
				that.globalData.cityCode = res.data.result.cityCodeStr

			}

		})

	},

	globalData: {
		userInfo: null,
		//host:'http://pet.ngrok.wiseimage.cn',
		// host:'http://pet.ngrok.wiseimage.cn',
		host: 'https://applet.rchongw.com', // 正式
	}
})