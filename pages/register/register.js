// pages/register/register.js
let app = getApp();

Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		registerMoney: ''
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		this.setData({
			registerMoney: opt.money
		})
	},

	/**
	 * 保存信息
	 */
	saveList() {
		let that = this,
			url = app.globalData.host + '/mobile/bountyHunters/save',
			obj = {
				realName: this.data.realName,
				phoneNum: this.data.phoneNum,
				wechatNum: this.data.wechatNum,
				email: this.data.email,
				bankNum: this.data.bankNum,
				registerMoney: this.data.registerMoney
			}

		if (!obj.realName){
			wx.showModal({
				title: '提示',
				content: '请输入名称',
			})
			return;
		} else if (!obj.phoneNum) {
			wx.showModal({
				title: '提示',
				content: '请输入手机号',
			})
			return;
		} else if (!obj.wechatNum) {
			wx.showModal({
				title: '提示',
				content: '请输入微信',
			})
			return;
		} else if (!obj.email) {
			wx.showModal({
				title: '提示',
				content: '请输入邮箱',
			})
			return;
		}

		if (!(/^1[0-9]{10}$/.test(obj.phoneNum))) {
			wx.showToast({
				title: '请填写正确的手机号码',
				icon: 'none'
			})
			return;
		}

		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {

				app.onLaunch();

				wx.showModal({
					title: '提示',
					content: '注册成功',
					success(res) {
						wx.reLaunch({
							url: '../index/index?register=1',
						})
					}
				})
			}
		})
	},

	realName(e){
		this.setData({
			realName: e.detail.value
		})
	},

	phoneNum(e){
		this.setData({
			phoneNum: e.detail.value
		})
	},

	wechatNum(e){
		this.setData({
			wechatNum: e.detail.value
		})
	},

	email(e){
		this.setData({
			email: e.detail.value
		})
	},

	bankNum(e){
		this.setData({
			bankNum: e.detail.value
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})