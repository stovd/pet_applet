// pages/vip/cart/cart.js 
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: [
			// { n: 1 }, { n: 1 }, { n: 1 },
		],
		ids: [],
		all: false,
		total: 0,	//　合计
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
	},
	getList(){
		let that = this,
			url = '/mobile/shoppingCart/findByMap'
		app.request('post', url, {}, res => {
			let list = res.data.result
			list.forEach(v=>{
				v.n = 1
			})

			that.setData({
				list: list,
			})
		})
	},

	// 去结算
	toPay(){
		let that = this,
			url = '/mobile/goodsOrder/submitOrder',
			list = this.data.list,
			ids = this.data.ids,
			arr = [],
			obj = {}

		ids.forEach(v=>{
			arr.push({
				goodsId: list[v].goodsId,
				goodsNum: list[v].n
			})
		})

		console.log(arr)
		obj = {
			goodsOrderCustomStr: JSON.stringify({ goodsOrderDetails: arr })
				
		}
		app.request('post', url, obj, res => {
			if(res.data.code === '200'){
				wx.navigateTo({
					url: '../order/order?o=' + encodeURIComponent(JSON.stringify(res.data.result)),
				})
			}else{
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})

	},
	// 全选
	selectAll(){
		if(!this.data.all){
			let arr = [], len = this.data.list.length, list = this.data.list, total = 0;
			for(let i = 0; i < len; i++){
				arr.push(i)
				total += (list[i].vipPrice * list[i].n)
			}
			this.setData({
				ids: arr,
				total: total
			})
		}else{
			this.setData({
				ids: [],
				total: 0
			})
		}

		this.setData({
			all: !this.data.all
		})
	},

	// 选择
	selectHandle(e){
		let ids = this.data.ids,
			idx = e.currentTarget.dataset.idx,
			list = this.data.list,
			total = this.data.total

		if(ids.indexOf(idx) === -1){
			ids.push(idx)
			total += (list[idx].vipPrice * list[idx].n)
		}else{
			ids.splice(ids.indexOf(idx), 1)
			total -= (list[idx].vipPrice * list[idx].n)
		}

		this.setData({
			ids: ids,
			total: total
		})
	},

	// 减
	minusHandle(e){
		let list = this.data.list,
			idx = e.currentTarget.dataset.idx,
			total = this.data.total,
			ids = this.data.ids,
			that = this

		if(list[idx].n > 1){
			list[idx].n -= 1

			if (ids.indexOf(idx) !== -1) {
				total -= list[idx].vipPrice
			}

			this.setData({
				list: list,
				total: total
			})
		}else{
			wx.showModal({
				title: '提示',
				content: '确定要移除商品吗？',
				success: (res)=>{
					console.log(res)
					if (res.confirm){
						that.delGoods(list[idx].goodsId)
					}
				}
			})
		}
	},
	// 加
	addHandle(e){
		let list = this.data.list,
			idx = e.currentTarget.dataset.idx,
			total = this.data.total,
			ids = this.data.ids
		
		list[idx].n += 1
		if (ids.indexOf(idx) !== -1) {
			total += list[idx].vipPrice
		}
		

		this.setData({
			list: list,
			total: total
		})
		
	},

	delGoods(id) {
		let that = this,
			url = '/mobile/shoppingCart/delByGoodsId'
		app.request('post', url, { goodsId: id}, res => {
			if(res.data.code === '200'){
				that.getList()
			}else{
				wx.showToast({
					title: '删除失败',
					icon: 'none'
				})
			}
		})
	},

	// 底部导航
	navHandle(e){
		wx.redirectTo({
			url: e.currentTarget.dataset.url,
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

})