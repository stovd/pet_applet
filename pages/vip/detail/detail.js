// pages/vip/detail/detail.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		collect: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		this.setData(JSON.parse(decodeURIComponent(opt.o)))
	},

	// 购买
	buyHndle(){
		let that = this,
			url = '/mobile/shoppingCart/findByMap'
		app.request('post', url, {}, res => {
			let list = res.data.result
			list.forEach(v => {
				v.n = 1
			})

			that.setData({
				list: list,
			})
		})
	},

	// 去结算
	toPay() {
		let that = this,
			url = '/mobile/goodsOrder/submitOrder',
			arr = [],
			obj = {}

		
		arr.push({
			goodsId: that.data.goodsId,
			goodsNum: 1
		})
		
		console.log(arr)
		obj = {
			goodsOrderCustomStr: JSON.stringify({ goodsOrderDetails: arr })

		}
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				wx.navigateTo({
					url: '../order/order?o=' + encodeURIComponent(JSON.stringify(res.data.result)),
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
	},

	// 加入购物车
	addCart(){
		let that = this,
			url = '/mobile/shoppingCart/save',
			obj = {
				goodsId: this.data.goodsId
			}
		
		app.request('post', url, obj, res=>{
			if(res.data.code === '200'){
				wx.showToast({
					title: '已添加到购物车',
					icon: 'success'
				})
			}else{
				wx.showToast({
					title: '加入购物车失败',
					icon: 'success'
				})
			}
		})

		
	},

	// s收藏
	collectHandle(){
		let txt = '收藏成功',
			that = this,
			url = '/mobile/collectGoods/save',
			obj = {
				goodsId: that.data.goodsId
			}

		if (this.data.isCollect == 2){
			txt = '取消收藏成功'
			url = '/mobile/collectGoods/delById'
			obj = {
				collectGoodsId: this.data.collectGoodsId
			}
		}
		

		app.request('post', url,
		 obj,
		 res=>{
			 if(res.data.code === '200'){
				 wx.showToast({
					 title: txt,
					 icon: 'success'
				 })
				 if (that.data.isCollect == 2){
					 that.setData({
						 collectGoodsId: '',
						 isCollect: 1
					 })
				 }else{
					 that.setData({
						 collectGoodsId: res.data.result.collectGoodsId,
						 isCollect: 2
					 })
				 }
				 
			 }else{
				 wx.showToast({
					 title: res.data.msg,
					//  icon: 'success'
				 })
			 }
		 })
			
		// this.setData({
		// 	collect: (!this.data.collect)
		// })
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

})