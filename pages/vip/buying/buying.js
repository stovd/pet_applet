// pages/buying/buying.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		navIndex: 1,
		pageNumber: 1,
		load: false,

		list: [],
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		//console.log(options)
		if (options.navIndex){
			this.setData({
				navIndex: options.navIndex
			})
		}

		
	},

	haswxinfo() {
		let that = this;
		if (app.globalData.userInfo && app.globalData.token) {
			console.log(app.globalData.userInfo)
			that.getList();
			
		} else {
			setTimeout(() => {
				that.haswxinfo();
			}, 1000)
		}
	},

	/**
	 *  获取列表
	 */ 
	getList(){
		this.setData({
			load: false
		})
		wx.showLoading({
			title: '正在拼命加载中...',
		})
		let that = this,
			url = '/mobile/goodsOrder/findByPage',
			obj = {
				state: this.data.navIndex == 1 ? '' : (this.data.navIndex - 1),
				current: this.data.pageNumber,
				size: 10
			}
		app.request('post', url, obj, function (res) {
			wx.hideLoading()
			that.setData({
				load: true
			})
			if (res.data.code === '200') {

				let list = res.data.result.records;

				if(list.length == 0){
					wx.showToast({
						title: '没有更多数据了',
					})
					that.setData({
						pageNumber: that.data.pageNumber == 1 ? 1 : (that.data.pageNumber - 1)
					})
					return;
				}
				
				for(let i=0; i < list.length; i++){
					list[i].createTime = list[i].createTime.slice(0, 8)
				}

				that.setData({
					list: that.data.list.concat(list)
				})
			}
		})
	},

	/**
	 * 导航
	 */
	navClick(e){
		this.setData({
			navIndex: e.target.dataset.id,
			list: []
		})

		this.getList();
	},

	// 详情
	toDetail(e){
		let idx = e.currentTarget.dataset.idx

		wx.navigateTo({
			url: '../order/order?o=' + encodeURIComponent(JSON.stringify(this.data.list[idx])),
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.setData({
			list: [],
			pageNumber: 1
		})
		this.haswxinfo();
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber +1
		})
		this.getList();
	},

})