// pages/vip/info/info.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		sex: 1,
		type: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		if(opt.type){
			this.setData({
				type: true
			})
		}
	},

	saveInfo(){
		let that = this,
			url = '/mobile/member/save',
			obj = {
				gender: this.data.sex,
				patriarchName: this.data.patriarchName,
				phoneNum: this.data.phoneNum,
				babyBreed: this.data.babyBreed,
				babyName: this.data.babyName,
				age: this.data.age,
				birthday: this.data.birthday
			}
		
		if (!obj.patriarchName){
			wx.showToast({
				title: '请填写家长姓名',
				icon: 'none'
			})
			return;
		} else if (!obj.phoneNum) {
			wx.showToast({
				title: '请填写手机号',
				icon: 'none'
			})
			return;
		} 
		// else if (!obj.babyBreed) {
		// 	wx.showToast({
		// 		title: '请填写宝贝品种',
		// 		icon: 'none'
		// 	})
		// 	return;
		// } else if (!obj.babyName) {
		// 	wx.showToast({
		// 		title: '请填写宝贝名字',
		// 		icon: 'none'
		// 	})
		// 	return;
		// } else if (!obj.age) {
		// 	wx.showToast({
		// 		title: '请填写年龄',
		// 		icon: 'none'
		// 	})
		// 	return;
		// } else if (!obj.birthday) {
		// 	wx.showToast({
		// 		title: '请填写性别',
		// 		icon: 'none'
		// 	})
		// 	return;
		// }


		app.request(
			'post',
			url,
			obj,
			res=>{
				if(res.data.code === '200'){
					wx.login({
						success: res => {
							// 发送 res.code 到后台换取 openId, sessionKey, unionId
							console.log(res)
							app.globalData.code = res.code;
							app.appLogin(res.code);
						},
						fail: err => {
							console.log(err)
						}
					})
					if(that.data.type){
						that.updateState()
						return;
					}
					wx.showModal({
						title: '提示',
						content: '加入成功',
						success: ()=>{
							wx.navigateTo({
								url: '../home/home',
							})
						}
					})
				}
			}
		)
	},

	updateState(){
		let that = this,
			url = '/mobile/user/updateById',
			obj = {
				memberState: 2
			}
		app.request('post', url, obj, res=>{
			if(res.data.code == '200'){
				wx.navigateTo({
					url: '../home/home',
				})
			}else{
				wx.showToast({
					title: '注册失败',
					icon: 'none'
				})
			}
		})
	},

	inputHandle(e){
		this.setData({
			[e.currentTarget.dataset.id]: e.detail.value
		})
	},

	// 立即加入
	subBtn(){
		wx.navigateTo({
			url: '../home/home',
		})
	},

	sexHandle(e){
		this.setData({
			sex: e.currentTarget.dataset.id
		})
	}

})