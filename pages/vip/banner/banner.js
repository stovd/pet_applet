// pages/vip/banner/banner.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		money: 0,
		memberState: '',
		scene: '',
		hasInfo: true
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		if (opt.scene) {
			let scene = decodeURIComponent(opt.scene);
			//&是我们定义的参数链接方式
			
			this.setData({
				scene: scene,
				memberState: scene.indexOf('&1') == -1 ? '0' : '1'
			})
			console.log('分享参数')
			console.log(scene)
			console.log('分享参数')

		}else{
			this.setData({
				memberState: app.globalData.memberState
			})
		}
		this.hasToken()
		
	},

	hasToken() {
		let that = this;
		if (app.globalData.token) {

			this.getList()
			
			if (!app.globalData.userInfo) {
				this.setData({
					hasInfo: false
				})
			}else if(that.data.scene){
				that.bindUser()
			}

		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	bindUser() {
		let that = this,
			url = '/mobile/user/updateById'
		app.request('post', url, { scene: that.data.scene }, res => {
			
		})
	},

	getUserInfo(e) {
		let that = this;
		console.log(e)
		if (e.detail.userInfo) {
			app.globalData.userInfo = e.detail.userInfo
			app.saveWXInfo(e.detail.userInfo)
			this.setData({
				userInfo: e.detail.userInfo,
				hasInfo: true
			})

			if(that.data.scene){
				that.bindUser()
			}
		}

	},


	getList() {
		let that = this,
			url = '/mobile/globalConfig/findById'
		app.request('post', url, {}, res => {
			that.setData({
				money: res.data.result.memberPrice
			})
		})
	},

	toHome(){
		if (this.data.memberState == '1'){
			wx.navigateTo({
				url: '../info/info?type=ty',
			})
			return;
		}
		wx.navigateTo({
			url: '../home/home',
		})
	},

	toPay() {
		// wx.navigateTo({
		// 	url: '../info/info',
		// })
		// return;
		let that = this,
			url = '/mobile/member/payMemberOrder'
		app.request('post', url, {}, res => {
			that.wxPay(res.data.result)
		})
	},

	wxPay(o) {
		let that = this;
		wx.requestPayment({
			timeStamp: o.timeStamp,
			nonceStr: o.nonceStr,
			package: o.package,
			signType: 'MD5',
			paySign: o.paySignStr,
			success(res) {
				console.log(res)
				if (res.errMsg === "requestPayment:ok") {
					wx.showModal({
						title: '提示',
						content: '支付成功！',
						success(res) {
							that.toInfo()
						}
					})
				}
			},
			fail(res) {
				wx.showToast({
					title: '支付失败',
					icon: 'none'
				})
			}
		})
	},

	toExplain(){
		wx.navigateTo({
			url: '../explain/explain',
		})
	},
	toInfo() {
		wx.navigateTo({
			url: '../info/info',
		})
	},


	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},


})