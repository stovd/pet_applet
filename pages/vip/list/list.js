// pages/vip/list/list.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		filter: [
			{ name: '品牌', id: 1, list: [
				{ id: 1, value: '老哈' }, { id: 2, value: '老黄' },
			] },
			{
				name: '适用品种', id: 1, list: [
					{ id: 1, value: '老哈' }, { id: 2, value: '老黄' },
				]
			},
			{
				name: '适用阶段', id: 1, list: [
					{ id: 1, value: '1-2岁' }, { id: 2, value: '2-3岁' },
				]
			},
			{
				name: '适用体型', id: 1, list: [
					{ id: 1, value: '小型犬' }, { id: 2, value: '大型犬' },
				]
			},
		],

		select0: '',
		select1: '',
		select2: '',
		select3: '',

		selectMask: false,
		category: '',
		searchKey: '',

		list: [],
		current: 1,
		lastPage: false,

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		if (opt.category){
			this.setData({
				category: opt.category
			})
		}else if(opt.brand){
			this.setData({
				select0: opt.brand
			})
		} else if (opt.searchKey){
			this.setData({
				searchKey: opt.searchKey
			})
		}
		this.getList()
		// this.getType(2)
		this.getBrand()
		this.getType(3)
		this.getType(4)
		this.getType(5)
		
	},

	hideMask(e){
		if(e.target.dataset.id === 'mask'){
			this.setData({
				selectMask: false
			})
		}
	},

	getType(id){
		let that = this,
			url = '/common/dicData/findDicDataByTypeId',
			obj = {
				dicTypId: id
			}
		app.request('post', url, obj, res=>{

			let list = res.data.result,
				arr = that.data.filter
			list.forEach(v=>{
				v.id = v.dicDataCode
				v.value = v.dicDataName
			})

			if(id == 2){
				arr[0].list = list
			}else if(id == 3){
				arr[1].list = list
			} else if (id == 4) {
				arr[2].list = list
			} else if (id == 5) {
				arr[3].list = list
			}
			
			that.setData({
				filter: arr
			})

		})
	},

	// 品牌列表
	getBrand() {
		let that = this,
			url = '/mobile/brand/findByAll'
		app.request('post', url, { dicTypId: 2 }, res => {
			let list = res.data.result,
				arr = that.data.filter
			list.forEach(v => {
				v.id = v.code
				v.value = v.brandName
			})
			arr[0].list = list
			that.setData({
				filter: arr
			})
		})
	},

	//获取列表
	getList() {
		if(this.data.lastPage) return;
		let that = this,
			url = '/mobile/goods/findByPage'
		app.request('post', url, {
			current: that.data.current,
			category: that.data.category,
			searchKey: that.data.searchKey,
			brand: that.data.select0,
			applyBreed: that.data.select1,
			applyStage: that.data.select2,
			applySize: that.data.select3,
		}, res => {
			let list = res.data.result.records

			if(list.length > 0){
				that.setData({
					current: that.data.current + 1,
					list: that.data.list.concat(list)
				})
			}else{
				wx.showToast({
					title: '没有更多数据了',
					icon: 'none'
				})
				that.setData({
					lastPage: true
				})
			}
			
			
		})
	},

	// 详情
	toDetail(e) {
		let obj = this.data.list[e.currentTarget.dataset.idx]

		obj = encodeURIComponent(JSON.stringify(obj))

		wx.navigateTo({
			url: '../detail/detail?o=' + obj,
		})
	},

	select0(e){
		let id = this.data.select0 === e.currentTarget.dataset.id ? 
			'' : e.currentTarget.dataset.id
		this.setData({
			select0: id
		})
	},
	select1(e) {
		let id = this.data.select1 === e.currentTarget.dataset.id ?
			'' : e.currentTarget.dataset.id
		this.setData({
			select1: id
		})
	},
	select2(e) {
		let id = this.data.select2 === e.currentTarget.dataset.id ?
			'' : e.currentTarget.dataset.id
		this.setData({
			select2: id
		})
	},
	select3(e) {
		let id = this.data.select3 === e.currentTarget.dataset.id ?
			'' : e.currentTarget.dataset.id
		this.setData({
			select3: id
		})
	},
	selectReset(){
		this.setData({
			select0: '',
			select1: '',
			select2: '',
			select3: ''
		})
	},
	selectConfirm(){
		this.setData({
			current: 1,
			lastPage: false,
			list: [],
			selectMask: false
		})
		this.getList()
	},
	select(){
		this.setData({
			selectMask: true
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.getList()
	},

})