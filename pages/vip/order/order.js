// pages/vip/order/order.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		infoDetail: {
			realName: '收货人信息',
			phoneNum: '',
			province: '',
			city: '',
			district: '',
			detailAddress: '',
			type: '',	// seckill 秒杀 group 拼团
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		if(opt.type){
			this.setData({
				type: opt.type,
				id: opt.id
			})
		}
		this.setData(JSON.parse(decodeURIComponent(opt.o)))
	},

	updateOrder() {
		let that = this,
			url = '/mobile/goodsOrder/updateStateById'
		app.request('post', url, { goodsOrderId: that.data.goodsOrderId, state: 5}, res => {
			if(res.data.code === '200'){
				wx.showModal({
					title: '提示',
					content: '取消成功',
					success: str=>{
						wx.navigateBack({
							delta: 1
						})
					}
				})
			}
		})
	},

	/**
	 * 个人信息数据
	 */
	getAddress(data) {
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = data || {}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				let region = [
					(res.data.result.province || '省'),
					(res.data.result.city || '市'),
					(res.data.result.district || '区')
				],
					regionCode = [
						(res.data.result.provinceCode || ''),
						(res.data.result.cityCode || ''),
						(res.data.result.districtCode || '')
					]
				that.setData({
					infoDetail: res.data.result,
					region: region,
					regionCode: regionCode
				})
			}
		})
	},

	// 获取支付参数
	getPayData(){
		if ((!this.data.infoDetail.phoneNum) || (!this.data.infoDetail.detailAddress)){
			wx.showToast({
				title: '请完善收货人信息',
				icon: 'none'
			})
			return;
		}
		let that = this,
			url = '/mobile/goodsOrder/payGoodsOrder'
		app.request('post', url, { goodsOrderId: that.data.goodsOrderId}, res => {
			let list = res.data.result
			that.wxPay(list)
		})
	},

	wxPay(o){
		let that = this;
		wx.requestPayment({
			timeStamp: o.timeStamp,
			nonceStr: o.nonceStr,
			package: o.package,
			signType: 'MD5',
			paySign: o.paySignStr,
			success(res) {
				console.log(res)
				if (res.errMsg === "requestPayment:ok") {
					wx.showModal({
						title: '提示',
						content: '支付成功！',
						success(res) {
							setTimeout(function () {
								// wx.navigateBack(-1)
								if (that.data.type == 'seckill'){
									wx.redirectTo({
										url: '../../seckill/seckillPage/seckillPage',
									})
									return;
								}else if(that.data.type == 'group'){
									wx.redirectTo({
										url: '../../seckill/groupPaySuccess/groupPaySuccess?id=' + that.data.id,
									})
									return;
								}
								wx.redirectTo({
									url: '../home/home',
								})
							}, 1000)

						}
					})
				}
			},
			fail(res) { 
				wx.showToast({
					title: '支付失败',
					icon: 'none'
				})
			}
		})
	},

	// 地址修改
	toInformation(){
		wx.navigateTo({
			url: '../../infomation/infomation',
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.getAddress()
	},


})