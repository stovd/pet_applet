// pages/vip/share/share.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		src: '../../../images/vip/QR_code_bg.png',
		src2: '../../../images/vip/QR_code_bg2.jpg'
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		let that = this, url = '';
		this.setData({
			id: opt.id
		})

		if(opt.id == 'hy'){
			url = '/getMemberShareQrCode'
		}else if(opt.id == 'ty'){
			url = '/getExperienceMemberShareQrCode'
		}
		wx.getImageInfo({
			src: app.globalData.host + url + '?token=' + app.globalData.token,    //请求的网络图片路径
			success: function (res) {
				console.log(res)
				console.log(res.path)
				//请求成功后将会生成一个本地路径即res.path,然后将该路径缓存到storageKeyUrl关键字中
				that.setData({
					codesrc: res.path
				})

				that.drawCanvas()

			}
		})
	},

	imgHandle(){
		let that = this

		wx.chooseImage({
			success(res) {
				wx.previewImage({
					current: res.tempFilePaths[0],
					urls: [res.tempFilePaths[0]],
				})
				
			}
		})
	},

	drawCanvas() {
		let that = this
		const ctx = wx.createCanvasContext('firstCanvas')
		if (this.data.id == 'hy') {
			ctx.drawImage('../../../images/vip/QR_code_bg.png', 0, 0, 300, 528)
		} else if (this.data.id == 'ty') {
			ctx.drawImage('../../../images/vip/QR_code_bg2.jpg', 0, 0, 300, 528)
		}

		ctx.drawImage(that.data.codesrc, 75, 110, 150, 150)

		ctx.draw(true, function () {
			that.dramimg(ctx)
		})
	},

	/**
	 *  导出图片
	 */
	dramimg(ctx) {
		let that = this
		console.log(222)
		wx.canvasToTempFilePath({
			x: 0,
			y: 0,
			width: 300,
			height: 528,
			destWidth: 640,
			destHeight: 880,
			canvasId: 'firstCanvas',
			success(res) {
				console.log(res.tempFilePath)
				that.setData({
					url: res.tempFilePath
				})

				wx.previewImage({
					current: res.tempFilePath, // 当前显示图片的http链接
					urls: [res.tempFilePath], // 需要预览的图片http链接列表
					complete: str => {
						console.log(str)
					}
				})
			}
		})
	},

	/**
	 *  预览图片
	 */
	preImg() {
		wx.previewImage({
			current: this.data.url, // 当前显示图片的http链接
			urls: [this.data.url], // 需要预览的图片http链接列表
			complete: str => {
				console.log(str)
			}
		})
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})