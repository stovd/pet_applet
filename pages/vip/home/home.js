// pages/vip/home/home.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: [],
		brandList: [],
		current: 1,
		lastPage: false,
		cartNum: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
		this.getBrand()
	},

	// 购物车商品
	getCartList() {
		let that = this,
			url = '/mobile/shoppingCart/findByMap'
		app.request('post', url, {}, res => {
			let list = res.data.result

			that.setData({
				cartNum: list.length,
			})
		})
	},

	// 品牌列表
	getBrand(){
		let that = this,
			url = '/mobile/brand/findByAll'
		app.request('post', url, { dicTypId: 2}, res=>{
			that.setData({
				brandList: res.data.result
			})
		})
	},

	getList() {
		if(this.data.lastPage) return;
		let that = this,
			url = '/mobile/goods/findByPage'
		app.request('post', url, {
			current: that.data.current
		}, res => {
			let list = res.data.result.records

			list.forEach(v => {
				
			})

			that.setData({
				list: that.data.list.concat(list),
				current: that.data.current + 1,
				lastPage: list.length === 0 ? true : false
			})
		})
	},

	// 搜索
	searchHandle(e){
		wx.navigateTo({
			url: '../list/list?searchKey=' + e.detail.value,
		})
	},

	// 品牌点击
	brandHandle(e){
		let id = e.currentTarget.dataset.id || ''
		wx.navigateTo({
			url: '../list/list?brand=' + id,
		})
	},

	// 详情
	toDetail(e){
		let obj = this.data.list[e.currentTarget.dataset.idx]

		obj = encodeURIComponent(JSON.stringify(obj))

		wx.navigateTo({
			url: '../detail/detail?o=' + obj,
		})
	},
	// 更多
	moreHandle(e){
		let id = e.currentTarget.dataset.type || ''
		wx.navigateTo({
			url: '../list/list?category=' + id,
		})
	},

	// 底部导航
	navHandle(e) {
		wx.redirectTo({
			url: e.currentTarget.dataset.url,
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.getCartList()
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.getList()
	},

})