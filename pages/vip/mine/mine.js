// pages/vip/mine/mine.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: {},
		memberState: ''
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.setData({
			memberState: app.globalData.memberState
		})
		this.getList()
	},
	getList(){
		let that = this,
			url = '/mobile/user/findById'

		app.request('post', url, {}, res=>{
			that.setData({
				list: res.data.result
			})
		})
	},

	/**
	 * 我买入的
	 */
	mineClidk(e) {
		wx.navigateTo({
			url: '../buying/buying?navIndex=' + e.currentTarget.dataset.id,
		})
	},

	// 我的收藏
	toCollect(){
		wx.navigateTo({
			url: '../collect/collect',
		})
	},

	// 分享
	toShare(e){
		wx.navigateTo({
			url: '../share/share?id=' + e.currentTarget.dataset.id,
		})
	},
	
	// 底部导航
	navHandle(e) {
		wx.redirectTo({
			url: e.currentTarget.dataset.url,
		})
	},


	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},


})