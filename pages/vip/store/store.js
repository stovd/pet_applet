// pages/vip/store/store.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: [],
		location: {}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		//this.getList()
		this.location()
	},
	//位置授权
	location() {
		let that = this;
		wx.getSetting({
			success(res) {
				if (!res.authSetting['scope.userLocation']) {
					wx.authorize({
						scope: 'scope.userLocation',
						success() {

							//获取位置信息
							wx.getLocation({
								type: 'wgs84',
								success(res) {
									console.log(res)
									that.setData({
										location: res
									})
									that.getList()
								}
							})

						},
						fail(err) {
							console.log('fail')
							that.getList()
						}
					})
				} else {
					//获取位置信息
					wx.getLocation({
						type: 'wgs84',
						success(res) {
							console.log(res)
							that.setData({
								location: res
							})
							that.getList()
						}
					})

				}
			}
		})

	},
	getList(){
		let that = this,
			url = '/mobile/store/findByMap'
		app.request('post', url, {
			latitude: that.data.location.latitude || '',
			longitude: that.data.location.longitude || ''
		}, res=>{
			let list = res.data.result

			list.forEach(v=>{
				v.labels = v.labels.split(',')
			})

			that.setData({
				list: list
			})
		})
	},

	makePhone(e){
		let phone = this.data.list[e.currentTarget.dataset.idx].phone
		wx.makePhoneCall({
			phoneNumber: phone,
		})
	},

	openMap(e){
		let o = this.data.list[e.currentTarget.dataset.idx]
		wx.openLocation({
			latitude: (o.latitude * 1),
			longitude: (o.longitude * 1),
			scale: 18
		})
	}

})