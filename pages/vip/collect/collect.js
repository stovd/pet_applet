// pages/vip/collect/collect.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: [],
		lastPage: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
	},
	getList() {
		let that = this,
			url = '/mobile/collectGoods/findByPage'

		app.request('post', url, { size: 1000}, res => {
			let list = res.data.result
			that.setData({
				list: that.data.list.concat(list),
				lastPage: list.length === 0 ? true : false
			})
		})
	},

	toDetail(e){
		let obj = this.data.list[e.currentTarget.dataset.idx]

		obj = encodeURIComponent(JSON.stringify(obj))

		wx.navigateTo({
			url: '../detail/detail?o=' + obj,
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},


	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

})