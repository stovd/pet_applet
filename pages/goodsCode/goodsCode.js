// pages/goodsCode/goodsCode.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		goodsImg: ''
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		let that = this;
		console.log(opt)
		let img = opt.img;
		console.log(img.indexOf('https'))

		if (img.indexOf('https') == -1){
			img = img.replace(/http/ig,'https')
		}
		console.log(img)

		wx.getImageInfo({
			src: app.globalData.host + '/getwxacodeunlimit?petId=' + opt.id +
				'&userId=' + app.globalData.userId +
				'&page=pages/detail1/detail1',    //请求的网络图片路径
			success: function (res) {
				console.log(res)
				console.log(res.path)
				//请求成功后将会生成一个本地路径即res.path,然后将该路径缓存到storageKeyUrl关键字中
				that.setData({
					codesrc: res.path,
					goodsName: opt.name,
					goodsPrice: opt.price
				})

				that.downGoodsImg(img)

				//that.drawCanvas()

			}
		})
	},

	/**
	 *  下载商品主图
	 */
	downGoodsImg(src){
		let that = this;
		wx.downloadFile({
			url: src, // 仅为示例，并非真实的资源
			success(res) {
				// 只要服务器有响应数据，就会把响应内容写入文件并进入 success 回调，业务需要自行判断是否下载到了想要的内容				
				console.log(res)
				if (res.statusCode === 200) {

					that.setData({
						goodsImg: res.tempFilePath
					})
					// wx.playVoice({
					// 	filePath: res.tempFilePath
					// })
					that.drawCanvas()
				}
			}
		})
	},

	

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		//this.drawCanvas();
	},

	drawCanvas() {
		let that = this
		const ctx = wx.createCanvasContext('firstCanvas')

		ctx.setFillStyle('#fff') // 填充颜色
		ctx.fillRect(0, 0, 300, 490)

		// ctx.drawImage('../../images/code_bg.png', 0, 0, 320, 440)
		ctx.drawImage( that.data.goodsImg , 10, 10, 280, 280) // 商品图

		ctx.drawImage(that.data.codesrc, 180, 300, 100, 100) // 二维码

		ctx.setFillStyle('#000')
		ctx.setFontSize(18)
		ctx.fillText(that.data.goodsName, 10, 320) // 商品名称
		ctx.fillText('￥'+that.data.goodsPrice, 10, 390) // 商品价格


		ctx.setFillStyle('#333')
		ctx.setFontSize(14)
		ctx.fillText('长按二维码保存到系统相册', 70, 420)

		ctx.draw(true, function () {
			that.dramimg(ctx)
		})
	},

	/**
	 *  导出图片
	 */
	dramimg(ctx) {
		let that = this
		console.log(222)
		wx.canvasToTempFilePath({
			x: 0,
			y: 0,
			width: 320,
			height: 440,
			destWidth: 640,
			destHeight: 880,
			canvasId: 'firstCanvas',
			success(res) {
				console.log(res.tempFilePath)
				that.setData({
					url: res.tempFilePath
				})

				wx.previewImage({
				    current: res.tempFilePath, // 当前显示图片的http链接
				    urls: [res.tempFilePath], // 需要预览的图片http链接列表
				    complete: str=>{
				        console.log(str)
				    }
				})
			}
		})
	},

	/**
	 *  预览图片
	 */
	preImg() {
		wx.previewImage({
			current: this.data.url, // 当前显示图片的http链接
			urls: [this.data.url], // 需要预览的图片http链接列表
			complete: str => {
				console.log(str)
			}
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})