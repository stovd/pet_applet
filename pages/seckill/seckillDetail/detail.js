// pages/vip/detail/detail.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		collect: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		this.setData({
			id: opt.id
		})
		this.getList()
		// this.setData(JSON.parse(decodeURIComponent(opt.o)))
	},

	getList(){
		let that = this,
			url = '/mobile/seckillTimeFrameGoods/findById',
			obj = {
				seckillTimeFrameGoodsId: that.data.id
			}
		app.request('post', url, obj, res => {
			let list = res.data.result
			// list.forEach(v => {
			// 	v.n = 1
			// })

			that.setData({
				list: list,
			})
		})
	},

	// 购买
	buyHndle(){
		let that = this,
			url = '/mobile/shoppingCart/findByMap'
		app.request('post', url, {}, res => {
			let list = res.data.result
			list.forEach(v => {
				v.n = 1
			})

			that.setData({
				list: list,
			})
		})
	},

	// 去结算
	toPay() {
		let that = this,
			url = '/mobile/goodsOrder/submitSeckillOrder',
			obj = {
				seckillTimeFrameGoodsId: that.data.list.seckillTimeFrameGoodsId
			}


		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				// return;
				wx.navigateTo({
					url: '../../vip/order/order?type=seckill&o=' + encodeURIComponent(JSON.stringify(res.data.result)) + '&id=' + that.data.id,
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
	},

	// 加入购物车
	addCart(){
		let that = this,
			url = '/mobile/shoppingCart/save',
			obj = {
				goodsId: this.data.list.goodsId
			}
		
		app.request('post', url, obj, res=>{
			if(res.data.code === '200'){
				wx.showToast({
					title: '已添加到购物车',
					icon: 'success'
				})
			}else{
				wx.showToast({
					title: '加入购物车失败',
					icon: 'success'
				})
			}
		})

		
	},

	// s收藏
	collectHandle(){
		let txt = '收藏成功',
			that = this,
			url = '/mobile/collectGoods/save',
			obj = {
				goodsId: that.data.list.goodsId
			}

		if (this.data.list.goodsCustom.isCollect == 2){
			txt = '取消收藏成功'
			url = '/mobile/collectGoods/delById'
			obj = {
				collectGoodsId: this.data.list.goodsCustom.collectGoodsId
			}
		}
		

		app.request('post', url,
		 obj,
		 res=>{
			 if(res.data.code === '200'){
				 wx.showToast({
					 title: txt,
					 icon: 'success'
				 })
				 that.getList()
				 
			 }else{
				 wx.showToast({
					 title: res.data.msg,
					//  icon: 'success'
				 })
			 }
		 })
			
		// this.setData({
		// 	collect: (!this.data.collect)
		// })
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

})