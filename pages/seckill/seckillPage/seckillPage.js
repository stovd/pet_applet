// pages/seckill/seckillPage/seckillPage.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		timeList: [],
		nav: 0,
		list: []
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
	},

	navHandle(e){
		this.setData({
			nav: e.currentTarget.dataset.idx
		})
		this.getGoodsList()
	},

	getList(){
		let that = this,
			url = app.globalData.host + '/mobile/seckillTimeFrame/findByMap',
			obj = {}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					timeList: res.data.result
				})
				that.getGoodsList()
			}
		})
	},

	getGoodsList() {
		let that = this,
			url = app.globalData.host + '/mobile/seckillTimeFrameGoods/findByMap',
			obj = {
				seckillTimeFrameId: that.data.timeList[that.data.nav].seckillTimeFrameId
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: res.data.result
				})
			}
		})
	},

	// 马上抢
	toPay(e) {
		let that = this,
			url = '/mobile/goodsOrder/submitSeckillOrder',
			obj = {
				seckillTimeFrameGoodsId: e.currentTarget.dataset.id
			}


		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				// return;
				wx.navigateTo({
					url: '../../vip/order/order?o=' + encodeURIComponent(JSON.stringify(res.data.result)),
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
	},

	toDetail(e){
		wx.navigateTo({
			url: '../seckillDetail/detail?id=' + e.currentTarget.dataset.id,
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})