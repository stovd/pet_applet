// pages/seckill/groupPaySuccess/groupPaySuccess.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		userInfo: {},
		list: {},
		num: 0
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		this.setData({
			id: opt.id,
			userInfo: app.globalData.userInfo
		})
		this.getList()
	},

	getList() {
		let that = this,
			url = '/mobile/groupBuyGoods/findUserById',
			obj = {
				groupBuyGoodsId: that.data.id
			}
		app.request('post', url, obj, res => {
			let list = res.data.result,
				num = list.groupBuyGoods.groupBuyLimitNum - list.userInfos.length

			num = num > 0 ? num : 0

			that.setData({
				list: list,
				num: num
			})
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {
		let that = this
		return {
			title: '瑞宠网',
			path: '/pages/seckill/groupDetail/detail?id=' + that.data.id
		}
	}
})