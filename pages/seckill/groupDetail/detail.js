// pages/vip/detail/detail.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		collect: false,
		list: {},
		hasInfo: true
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		// this.setData(JSON.parse(decodeURIComponent(opt.o)))
		this.setData({
			id: opt.id
		})
		//this.getList()
		this.hasToken()
	},

	hasToken() {
		let that = this;
		if (app.globalData.token) {
			this.getList()
			if (!app.globalData.userInfo) {
				this.setData({
					hasInfo: false
				})
			}
		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	getUserInfo(e) {
		let that = this;
		console.log(e)
		if (e.detail.userInfo) {
			app.globalData.userInfo = e.detail.userInfo
			app.saveWXInfo(e.detail.userInfo)
			this.setData({
				userInfo: e.detail.userInfo,
				hasInfo: true
			})
		}

	},

	// 拼团
	groupPay(e) {
		let that = this,
			url = '/mobile/goodsOrder/submitGroupBuyOrder',
			obj = {
				groupBuyGoodsId: e.currentTarget.dataset.id
			}


		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				if (res.data.msg == 'n') {
					wx.navigateTo({
						url: '../groupPaySuccess/groupPaySuccess?id=' + e.currentTarget.dataset.id,
					})
					return;
				}
				// return;
				wx.navigateTo({
					url: '../../vip/order/order?type=group&o=' + encodeURIComponent(JSON.stringify(res.data.result)) + '&id=' + e.currentTarget.dataset.id,
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
	},

	getList() {
		let that = this,
			url = '/mobile/groupBuyGoods/findById',
			obj = {
				groupBuyGoodsId: that.data.id
			}
		app.request('post', url, obj, res => {
			let list = res.data.result
			
			that.setData({
				list: list,
			})
		})
	},

	// 购买
	buyHndle(){
		let that = this,
			url = '/mobile/shoppingCart/findByMap'
		app.request('post', url, {}, res => {
			let list = res.data.result
			list.forEach(v => {
				v.n = 1
			})

			that.setData({
				list: list,
			})
		})
	},

	// 去结算
	toPay() {
		let that = this,
			url = '/mobile/goodsOrder/submitOrder',
			arr = [],
			obj = {}

		
		arr.push({
			goodsId: that.data.list.goodsId,
			goodsNum: 1
		})
		
		console.log(arr)
		obj = {
			goodsOrderCustomStr: JSON.stringify({ goodsOrderDetails: arr })

		}
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				wx.navigateTo({
					url: '../../vip/order/order?o=' + encodeURIComponent(JSON.stringify(res.data.result)),
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
		// wx.navigateTo({
		// 	url: '../cart/cart',
		// })
	},

	// 加入购物车
	addCart(){
		let that = this,
			url = '/mobile/shoppingCart/save',
			obj = {
				goodsId: this.data.list.goodsId
			}
		
		app.request('post', url, obj, res=>{
			if(res.data.code === '200'){
				wx.showToast({
					title: '已添加到购物车',
					icon: 'success'
				})
			}else{
				wx.showToast({
					title: '加入购物车失败',
					icon: 'success'
				})
			}
		})

		
	},

	// s收藏
	collectHandle(){
		let txt = '收藏成功',
			that = this,
			url = '/mobile/collectGoods/save',
			obj = {
				goodsId: that.data.list.goodsId
			}

		if (this.data.list.goodsCustom.isCollect == 2){
			txt = '取消收藏成功'
			url = '/mobile/collectGoods/delById'
			obj = {
				collectGoodsId: this.data.list.goodsCustom.collectGoodsId
			}
		}
		

		app.request('post', url,
		 obj,
		 res=>{
			 if(res.data.code === '200'){
				 wx.showToast({
					 title: txt,
					 icon: 'success'
				 })
				 if (that.data.isCollect == 2){
					 that.setData({
						 collectGoodsId: '',
						 isCollect: 1
					 })
				 }else{
					 that.setData({
						 collectGoodsId: res.data.result.collectGoodsId,
						 isCollect: 2
					 })
				 }
				 that.getList()
				 
			 }else{
				 wx.showToast({
					 title: res.data.msg,
					//  icon: 'success'
				 })
			 }
		 })
			
		// this.setData({
		// 	collect: (!this.data.collect)
		// })
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

})