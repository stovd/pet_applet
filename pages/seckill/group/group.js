// pages/seckill/group/group.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list: [],
		current: 1
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
	},


	// 拼团
	toPay(e) {
		let that = this,
			url = '/mobile/goodsOrder/submitGroupBuyOrder',
			obj = {
				groupBuyGoodsId: e.currentTarget.dataset.id
			}


		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				if(res.data.msg == 'n'){
					wx.navigateTo({
						url: '../groupPaySuccess/groupPaySuccess?id=' + e.currentTarget.dataset.id,
					})
					return;
				}
				
				wx.navigateTo({
					url: '../../vip/order/order?o=' + encodeURIComponent(JSON.stringify(res.data.result)),
				})
			} else {
				wx.showToast({
					title: '服务异常' + res.data.msg,
				})
			}
		})
	},

	getList() {
		let that = this,
			url = '/mobile/groupBuyGoods/findByPage',
			obj = {
				current: that.data.current
			}
		app.request('post', url, obj, res => {
			let list = res.data.result.records
			// list.forEach(v => {
			// 	v.n = 1
			// })
			if(list.length === 0){
				wx.showToast({
					title: '没有更多了',
					icon: 'none'
				})
				return;
			}

			that.setData({
				current: that.data.current + 1,
				list: that.data.list.concat(list),
			})
		})
	},

	toDetail(e){
		let that = this,
			url = '/mobile/goodsOrder/checkGroupBuyOrder',
			obj = {
				groupBuyGoodsId: e.currentTarget.dataset.id
			}
		app.request('post', url, obj, res => {
			if(res.data.result === 'n'){
				wx.navigateTo({
					url: '../groupDetail/detail?id=' + e.currentTarget.dataset.id,
				})
			} else if (res.data.result === 'y'){
				wx.navigateTo({
					url: '../groupPaySuccess/groupPaySuccess?id=' + e.currentTarget.dataset.id,
				})
			}
			
		})
		return;
		
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.getList()
	},

})