// pages/collect/collect.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		list:[],
		pageNumber: 1,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList();
	},

	/**
	 * 获取列表
	 */
	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/collect/findByPage',
			obj = {
				size: 50,
				current: this.data.pageNumber
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: res.data.result.records
				})
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber ++
		})
		this.getList();
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})