// pages/merchant/merchant.js
let app = getApp();

Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		hasInfo: true,
		pageNumber: 1,
		list: [],
		info: {},
		total: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {

		// 小程序二维码进入
		if (opt.scene) {
			let scene = decodeURIComponent(opt.scene);
			//&是我们定义的参数链接方式
			let userId = scene.split("&")[0];
			let petId = scene.split('&')[1];
			this.setData({
				petId: petId,
				shareUserId: userId,
			})

		}else if(opt.id){
			this.setData({
				shareUserId: userId,
			})
		}

		this.hasToken()

	},

	hasToken() {
		let that = this;

		if (app.globalData.token) {
			
			this.getList();
			this.getInfo();

			// 判断是否已授权获取用户信息
			if (!app.globalData.userInfo) {
				this.setData({
					hasInfo: false
				})
			}
			
			// 二维码进来
			if (this.data.shareUserId){
				app.location()
				this.bindRecommend();
			}
			

		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	/**
	 *  新用户获取授权信息
	 */
	getUserInfo(e) {
		let that = this;
		console.log(e)
		if (e.detail.userInfo) {
			app.globalData.userInfo = e.detail.userInfo
			app.saveWXInfo(e.detail.userInfo)
			this.setData({
				userInfo: e.detail.userInfo,
				hasInfo: true
			})
		}

	},

	/**
	 * 关注
	 */
	focus() {
		let that = this,
			url = app.globalData.host + '/mobile/focus/save',
			obj = {
				userId: app.globalData.userId 
			}
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				wx.showToast({
					title: '关注成功',
				})
			}
		})
	},

	/**
	 * 商品详情
	 */
	toDetail(e) {
		wx.navigateTo({
			url: '../detail1/detail1?id=' + e.currentTarget.dataset.id,
		})
	},



	/**
	 *  分享二维码进入绑定推荐关系
	 */
	bindRecommend() {
		
		let that = this,
			url = app.globalData.host + '/mobile/user/updateById',
			obj = {
				recommendUserId: that.data.shareUserId || 1,
				userId: app.globalData.userId || 2
			}

		if ((!app.globalData.cityCode) || (!app.globalData.userInfo)) {
			setTimeout(function () {
				that.bindRecommend();
			}, 1000)
			return;
		}

		app.request('post', url, obj, res => {
			if (res.data.code === '200') {

			}
		})
	},

	/**
 *  获取商品列表
 */
	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/collect/findByPage',
			obj = {
				size: 50,
				current: this.data.pageNumber,
				editUserId: this.data.shareUserId || ''
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: that.data.list.concat(res.data.result.records)
				})
			}
		})
	},

	/**
	 * 我的信息
	 */
	getInfo() {
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = {
				editUserId: this.data.shareUserId || ''
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					info: res.data.result
				})
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber + 1
		})
		this.getList()
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})