// pages/buying/buying.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		navIndex: 1,
		pageNumber: 1,

		list: [],
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		//console.log(options)
		if (options.navIndex){
			this.setData({
				navIndex: options.navIndex
			})
		}

		this.haswxinfo();
	},

	haswxinfo() {
		let that = this;
		if (app.globalData.userInfo && app.globalData.token) {
			console.log(app.globalData.userInfo)
			that.getList();
			
		} else {
			setTimeout(() => {
				that.haswxinfo();
			}, 1000)
		}
	},

	/**
	 *  获取列表
	 */
	getList(){
		let that = this,
			url = app.globalData.host + '/mobile/order/findByPage',
			obj = {
				state: this.data.navIndex == 1 ? '' : (this.data.navIndex - 1),
				current: this.data.pageNumber,
				size: 50
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {

				let list = res.data.result.records;

				if(list.length == 0){
					wx.showToast({
						title: '没有更多数据了',
					})
					that.setData({
						pageNumber: that.data.pageNumber == 1 ? 1 : (that.data.pageNumber - 1)
					})
					return;
				}
				
				for(let i=0; i < list.length; i++){
					list[i].createTime = list[i].createTime.slice(0, 8)
				}

				that.setData({
					list: that.data.list.concat(list)
				})
			}
		})
	},

	/**
	 * 导航
	 */
	navClick(e){
		this.setData({
			navIndex: e.target.dataset.id,
			list: []
		})

		this.getList();
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber +1
		})
		this.getList();
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})