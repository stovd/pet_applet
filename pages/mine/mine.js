// pages/mine/mine.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
	  userinfo:{},
	  list:{}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
	  this.hasToken();
  },

	hasToken() {
		let that = this;

		if (app.globalData.token) {

			//判断是否是赏金猎人
			if (app.globalData.hunters){
				wx.redirectTo({
					url: '../lieren/lieren',
				})
				return;
			}
			
			that.setData({
				userinfo: app.globalData.userInfo
			})

			this.getList();

		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	toHelp(){
		wx.navigateTo({
			url: '../help/help',
		})
	},

	/**
	 * 底部导航
	 */
	footerNav(){
		wx.redirectTo({
			url: '../index/index',
		})
	},

	getList(){
		let that = this,
			url = app.globalData.host + '/mobile/user/findSimplenessById',
			obj = {
				
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: res.data.result
				})
			}
		})
	},

  /**
   * 个人信息
   */
	infomation(){
		wx.navigateTo({
			url: '../infomation/infomation',
		})
	},

	/**
	 * 我买入的
	 */
	mineClidk(e){
		wx.navigateTo({
			url: '../vip/buying/buying?navIndex=' + e.currentTarget.dataset.id,
		})
		// wx.navigateTo({
		// 	url: '../buying/buying?navIndex=' + e.currentTarget.dataset.id,
		// })
	},

	/**
	 * 收藏
	 */
	collectClick(){
		wx.navigateTo({
			url: '../collect/collect',
		})
	},

	/**
	 * 我的关注
	 */
	attentionClick(){
		wx.navigateTo({
			url: '../attention/attention',
		})
	},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})