// pages/infomation/infomation.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		region:['省','市','区'],
		regionCode:['', '', ''],
		customItem:'',

		infoDetail:{ // 信息详情

		}

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

		this.hasToken();
	},

	hasToken() {
		let that = this;

		if (app.globalData.token) {
			this.pageData();
		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	/**
	 * 保存信息
	 */
	saveInfo(){
		let obj = this.data.infoDetail;
		obj.province = this.data.region[0];
		obj.provinceCode = this.data.regionCode[0];
		obj.city = this.data.region[1];
		obj.cityCode= this.data.regionCode[1];
		obj.district = this.data.region[2];
		obj.districtCode = this.data.regionCode[2];
		console.log(obj)
		//this.pageData(obj);
		console.log(/^1[0-9]{10}$/.test(obj.phoneNum))

		if (!obj.realName){
			wx.showToast({
				title: '收货人姓名不能为空',
				icon: 'none'
			})
			return;
		} else if (!obj.phoneNum) {
			wx.showToast({
				title: '手机号码不能为空',
				icon: 'none'
			})
			return;
		} else if (!obj.district) {
			wx.showToast({
				title: '请选择收货地址',
				icon: 'none'
			})
			return;
		} else if (!obj.detailAddress) {
			wx.showToast({
				title: '请填写详细地址',
				icon: 'none'
			})
			return;
		}

		if (!(/^1[0-9]{10}$/.test(obj.phoneNum))) {
			wx.showToast({
				title: '请填写正确的手机号码',
				icon: 'none'
			})
			return;
		}


		let that = this,
			url = app.globalData.host + '/mobile/user/updateById';
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				wx.showToast({
					title: '保存成功',
				})

				setTimeout(()=>{
					wx.navigateBack({ delta:1})
				},1000)
			}
		})

	},

	/**
	 * 手机号码
	 */
	phoneNum(e){
		let obj = this.data.infoDetail;
		obj.phoneNum = e.detail.value;
		this.setData({
			infoDetail: obj
		})
	},

	/**
	 * 详细地址
	 */
	detailAddress(e){
		let obj = this.data.infoDetail;
		obj.detailAddress = e.detail.value;
		this.setData({
			infoDetail: obj
		})
	},

	/**
	 * 收货人姓名
	 */
	realName(e){
		//console.log(e)
		let obj = this.data.infoDetail;
		obj.realName = e.detail.value;
		this.setData({
			infoDetail: obj
		})
	},

	/**
	 * 个人信息数据
	 */
	pageData(data){
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = data || {}
		app.request('post', url, obj, function(res){
			if(res.data.code === '200'){
				let region = [
					(res.data.result.province || '省'),
					(res.data.result.city || '市'),
					(res.data.result.district || '区')
				],
					regionCode = [
						(res.data.result.provinceCode ||''),
						(res.data.result.cityCode || ''),
						(res.data.result.districtCode || '')
					]
				that.setData({
					infoDetail: res.data.result,
					region: region,
					regionCode: regionCode
				})
			}
		})
	},

	/**
	 * 省市选择
	 */
	bindRegionChange(e){
		console.log(e)
		this.setData({
			region: e.detail.value,
			regionCode: e.detail.code
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})