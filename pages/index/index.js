//index.js
//获取应用实例
const app = getApp()

Page({
    data: {
        userInfo: {},
        hasUserInfo: false,
        canIUse: wx.canIUse('button.open-type.getUserInfo'),
	    autoplay:true,
		interval:2000,
		duration:300,
		circular:true,
		imgUrls:[
			'../../images/65.png',
			'../../images/65.png'
		],

		//轮播
		bannerList:[],

		//搜索关键词
		searchvalue:'',

		//猎人
		huntsman:false,

		//位置
		haslocation: true,
		city: '',
		cityCode: '',

		//商品列表
		goodsList:{
			newPets:[],//新品上架
			recommendPets:[]//精品推荐
		},

		loading: true,

    },

	seckillHandle(){
		wx.navigateTo({
			url: '../seckill/seckillPage/seckillPage',
		})
	},
	groupHandle(){
		wx.navigateTo({
			url: '../seckill/group/group',
		})
	},

	/**
	 * 底部导航
	 */
	footerNav(){
		// wx.navigateTo({
		// 	url: '../mine/mine',
		// })

		if (app.globalData.ismember || app.globalData.memberState == '2') {
			wx.navigateTo({
				url: '../vip/mine/mine',
			})
		} else {
			wx.navigateTo({
				url: '../mine/mine',
			})
		}

		return;

		//判断是否是赏金猎人
		if (app.globalData.hunters){
			wx.redirectTo({
				url: '../lieren/lieren',
			})
			return;
		}else{
			wx.redirectTo({
				url: '../mine/mine',
			})
		}
	},

	/**
	 * 搜索类目
	 */
	search(){
		wx.navigateTo({
			url: '../category/category?key=' + this.data.searchvalue,
		})
		// wx.navigateTo({
		// 	url: '../animal/animal?key=' + this.data.searchvalue,
		// })
	},

	/**
	 * 搜索按键词
	 */
	searchVal(e){
		// console.log(e)
		this.setData({
				searchvalue: e.detail.value
			})
	},

	/**
	 * 类目导航
	 */
	category(e){
		console.log(e.currentTarget.dataset.id)
		let id = e.currentTarget.dataset.id;

		if(id === 'store'){
			wx.navigateTo({
				url: '../vip/store/store',
			})
		}else if(id === 'vip'){
			// 会员
			
			if (app.globalData.ismember){
				wx.navigateTo({
					url: '../vip/home/home',
				})
			}else{
				wx.navigateTo({
					url: '../vip/banner/banner',
				})
			}
			
		}else{
			wx.navigateTo({
				url: '../category/category?type=' + id,
			})
		}

		
	},

	/**
	 * 成为赏金猎人
	 */
	huntsman(){
		if (this.data.hunters){
			wx.navigateTo({
				url: '../lieren/lieren',
			})
		}else{
			wx.navigateTo({
				url: '../huntsman/huntsman',
			})
		}
		
	},

	/**
	 * 最新上架更多
	 */
	newMore(){
		wx.navigateTo({
			url: '../newGoods/newGoods?id=new',
		})
	},

	/**
	 *  精品推荐
	 */
	boutique(){
		wx.navigateTo({
			url: '../newGoods/newGoods?id=boutique',
		})
	},

	/**
	 * 商品详情
	 */
	goodsDetail(id){
		console.log(id.currentTarget.dataset.id)
		wx.navigateTo({
			url: '../detail1/detail1?id=' + id.currentTarget.dataset.id,
		})
	},


	onLoad: function (opt) {

		if (app.globalData.userInfo) {
			this.setData({
				userInfo: app.globalData.userInfo,
				hasUserInfo: true
			})
		} else if (this.data.canIUse){
			// 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
			// 所以此处加入 callback 以防止这种情况
			app.userInfoReadyCallback = res => {
				this.setData({
				userInfo: res.userInfo,
				hasUserInfo: true
				})
			}
		} else {
			// 在没有 open-type=getUserInfo 版本的兼容处理
			wx.getUserInfo({
				success: res => {
				app.globalData.userInfo = res.userInfo
				this.setData({
					userInfo: res.userInfo,
					hasUserInfo: true
				})
				}
			})
		}

		this.haswxinfo();

	},

	haswxinfo(){
		// console.log('111111111111')
		let that = this;
		if(app.globalData.userInfo && app.globalData.token){
			console.log(app.globalData.userInfo)

			// wx.showLoading({
			// 	title: '加载中',
			// })

			// wx.showToast({
			// 	title: '',
			// 	icon: 'none',
			// 	image: '../../images/dog.gif',
			// 	duration: 10000
			// })

			this.setData({
				hunters: app.globalData.hunters
			})

			that.saveWXInfo();
			// that.location();
			that.indexGoods();
			that.indexBanner();
		}else{
			setTimeout(()=>{
				that.haswxinfo();
			},1000)
		}
	},

	/**
	 * 保存微信昵称和头像
	 */
	saveWXInfo(){
		let that = this,
			url = app.globalData.host + '/mobile/user/updateById',
			obj = {
				nickname: app.globalData.userInfo.nickName,
				headImgPath: app.globalData.userInfo.avatarUrl,
				token: app.globalData.token
			}
		app.request('post', url, obj, function(res){
			if(res.data.code === '200'){

			}
		})
	},

	/**
	 * 最新上架 精品推荐
	 */
	indexGoods(){

		let that = this,
			url = app.globalData.host + '/mobile/pet/findPetByIndex',
			obj = {
				current: 1,
				size: 10,
				city: this.data.cityCode
			}
		
		app.request('post', url, obj, function(res){
			//console.log(res)
			that.data.loading = false;
			if(res.data.code === '200'){
				that.setData({
					goodsList: res.data.result
				})
			}



			// wx.hideLoading()

		})
	},

	/**
	 * 客服
	 */
	handleContact(e) {
		console.log(e)
		//console.log(e.path)
		//console.log(e.query)
	},

	//轮播
	indexBanner(){
		let that = this,
			url = app.globalData.host + '/mobile/banner/findByMap',
			obj = {

			}
		app.request('post',url,obj,function(res){

			if(res.data.code === '200'){
				that.setData({
					bannerList: res.data.result
				})
			}

		})
	},

	//获取位置城市
	getCity(v){
		
		let that = this,
			url = app.globalData.host + '/position',
			obj = {
				latitude: v.latitude,
				longitude: v.longitude
			}
		app.request('post', url, obj, function(res){

			if(res.data.code === '200'){
				that.setData({
					city: res.data.result.cityStr,
					cityCode: res.data.result.cityCodeStr
				})

				app.globalData.cityCode = res.data.result.cityCodeStr

				that.indexGoods();
			}

		})

	},

	//位置授权
	location(){
		let that = this;
		wx.getSetting({
			success(res) {
				if (!res.authSetting['scope.userLocation']) {
					wx.authorize({
						scope: 'scope.userLocation',
						success() {
						
							//获取位置信息
							wx.getLocation({
								type: 'wgs84',
								success(res) {
									console.log(res)
									that.getCity(res)
								}
							})
							
						},
						fail(err){
							console.log('fail')
							that.setData({
								haslocation: false
							})
						}
					})
				}else{
					//获取位置信息
					wx.getLocation({
						type: 'wgs84',
						success(res) {
							console.log(res)
							that.getCity(res)
						}
					})

				}
			}
		})

	},
	//打开权限设置
	openSetting(){
		let that = this;
		wx.openSetting({
			success(res) {
				console.log(res.authSetting)
				that.setData({
					haslocation: true
				})

				that.location();
			}
		})

	},

	getUserInfo: function(e) {
			console.log(e)
		if (e.detail.userInfo){
			app.globalData.userInfo = e.detail.userInfo	
			this.setData({
				userInfo: e.detail.userInfo,
				hasUserInfo: true
			})
		}
		
	},

	/**
   * 用户点击右上角分享
   */
	onShareAppMessage: function () {

	}
})
