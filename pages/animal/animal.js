// pages/animal/animal.js
let app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
	  navIndex:'1',

	  key: '',
	  list: [],
	  pageSize: 100,
	  pageNumber: 1,

	  gender: '',
	  orderByVariety: '',
	  orderByPrice: '',

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (opt) {
	  console.log(opt)
	  if (opt.id) {
		  this.setData({
			  key: opt.id
		  })
	  }
	  //宠物列表
	  this.petList();
  },

	/**
		 * 宠物列表
		 */
	petList() {
		let that = this,
			url = app.globalData.host + '/mobile/pet/findByPage',
			obj = {
				current: this.data.pageNumber,
				size: this.data.pageSize,
				categoryId: this.data.key,
				gender: this.data.gender,
				orderByVariety: this.data.orderByVariety,
				orderByPrice: this.data.orderByPrice,
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				let list = that.data.list,
					pageNumber;
				
				if (res.data.result.records.length > 0){
					pageNumber = that.data.pageNumber + 1
				}else{
					pageNumber = that.data.pageNumber

					wx.showToast({
						title: '没有更多数据了',
					})
				}

				list = list.concat(res.data.result.records)
				that.setData({
					list: list,
					pageNumber: pageNumber
				})
			}
		})

	},

  /**
   * 详情
   */
	toDetail(e){
		wx.navigateTo({
			url: '../detail1/detail1?id=' + e.currentTarget.dataset.id,
		})
	},

  /**
   * 导航
   */
	animalNav(e){
		if (e.currentTarget.dataset.id == 1){
			this.setData({
				navIndex: e.currentTarget.dataset.id,
				gender: this.data.gender == '' ? 1 : this.data.gender == 1 ? 2 : ''
			})
		} else if (e.currentTarget.dataset.id == 2) {
			this.setData({
				navIndex: e.currentTarget.dataset.id,
				orderByPrice: '',
				orderByVariety: this.data.orderByVariety == '' ? 1 : this.data.orderByVariety == 1 ? 2 : ''
			})
		} else if (e.currentTarget.dataset.id == 3) {
			this.setData({
				navIndex: e.currentTarget.dataset.id,
				orderByVariety: '',
				orderByPrice: this.data.orderByPrice == '' ? 1 : this.data.orderByPrice == 1 ? 2 : ''
			})
		} 

		this.setData({
			pageNumber: 1,
			list: []
		})

		this.petList()
		
	},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
	  console.log(22222)
	  this.petList();
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})