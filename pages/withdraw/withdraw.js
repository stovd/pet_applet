// pages/withdraw/withdraw.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		total: 0,
		value: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getList()
	},

	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = {

			}
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {

				that.setData({
					total: res.data.result.balance /100,
					value: res.data.result.balance / 100,
				})

			}
		})
	},

	inpuValue(e){
		//console.log(e)

		this.setData({
			value: e.detail.value
		})
	},

	/**
	 *  提现
	 */
	widthdraw() {
		let that = this,
			url = app.globalData.host + '/mobile/moneyRecord/withDraw',
			obj = {
				money: parseInt(this.data.value * 100),
				type: 1
			}

		

		

			console.log(obj.money)
			console.log(isNaN(obj.money))

			if(!obj.money || isNaN(obj.money)){
				wx.showModal({
					title: '提示',
					content: '请输入正确的金额',
				})
				return;
			}

		if (obj.money > this.data.total) {
			wx.showModal({
				title: '提示',
				content: '提现余额不足',
			})
			return;
		}

		app.request('post', url, obj, res => {
			if (res.data.code === '200') {

				wx.showToast({
					title: '提现成功',
				})
				that.setData({
					value:''
				})
				that.getList();
			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})