// pages/category/category.js
let app = getApp()
Page({

  /**
   * 页面的初始数据
   */
  data: {
	  key: '',
	  type: '',
	  list: [],
	  pageSize: 1000,
	  pageNumber: 1,
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (opt) {
	  console.log(opt)
	  if(opt.key){
		  this.setData({
			  key: opt.key
		  })
	  }
	  if(opt.type){
		  this.setData({
			  type: opt.type
		  })
	  }
	  //宠物列表
	  this.petList();
  },

	/**
	   * 宠物列表
	   */
	petList() {
		let that = this,
			url = app.globalData.host + '/mobile/category/findByMap',
			obj = {
				current: this.data.pageNumber,
				size: this.data.pageSize,
				title: this.data.key,
				type: this.data.type
			}

		app.request('post', url, obj, function (res) {
			if(res.data.code === '200'){
				that.setData({
					list: res.data.result
				})
			}
		})

	},

  /**
   * 点击跳转
   */
	animalList(e){
		wx.navigateTo({
			url: '../animal/animal?id=' + e.currentTarget.dataset.id,
		})
	},

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})