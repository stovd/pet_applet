// pages/detail1/detail1.js
//获取应用实例
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		hasInfo: true,
		petId: '',//商品id
		list:['一','二','三'],
		hunters: '',
		shareUserId: '',

		goodsData: { price: 0 },//商品信息

		share: false
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)

		// 小程序二维码进入
		if (opt.scene) {
			let scene = decodeURIComponent(opt.scene);

			console.log(scene)
			// return;
			//&是我们定义的参数链接方式
			let userId = scene.split("&")[0];
			let petId = scene.split('&')[1];
			this.setData({
				petId: petId,
				shareUserId: userId,
				share: true,
				//hunters: app.globalData.hunters
			})
			
		}else{
			this.setData({
				petId: opt.id,
				//hunters: app.globalData.hunters
			})
		}

		

		this.hasToken();
		//this.pageData();
	},

	hasToken(){
		let that = this;
		
		if(app.globalData.token){
			
			this.pageData();

			// 判断是否已授权获取用户信息
			if (!app.globalData.userInfo) {
				this.setData({
					hasInfo: false
				})
			}

			// 绑定推荐人
			if (this.data.shareUserId) {
				app.location()
				this.bindRecommend();
			}
		
			
			this.setData({
				hunters: app.globalData.hunters
			})
		}else{
			setTimeout(()=>{
				that.hasToken();
			},1000)
		}
	},

	/**
	 *  新用户获取授权信息
	 */
	getUserInfo(e) {
		let that = this;
		console.log(e)
		if (e.detail.userInfo) {
			app.globalData.userInfo = e.detail.userInfo
			app.saveWXInfo(e.detail.userInfo)
			this.setData({
				userInfo: e.detail.userInfo,
				hasInfo: true
			})
		}

	},

	/**
	 * 分享二维码
	 */
	shareCode(e) {

		wx.navigateTo({
			url: '../goodsCode/goodsCode?id=' + this.data.petId + '&img=' + this.data.goodsData.imgPath +
				'&name=' + this.data.goodsData.petName + '&price=' + this.data.goodsData.price / 100,
		})
	},

	/**
	 *  分享二维码进入绑定推荐关系
	 */
	bindRecommend(){

		let that = this,
			url = app.globalData.host + '/mobile/user/updateById',
			obj = {
				recommendUserId: that.data.shareUserId || 1,
				// userId: app.globalData.userId || 2 
			}

		if ((!app.globalData.cityCode) || (!app.globalData.userInfo)) {
			setTimeout(function () {
				that.bindRecommend();
			}, 1000)
			return;
		}
		
		app.request('post', url, obj, res=>{
			if(res.data.code === '200'){

			}
		})
	},

	/**
	 * 收藏/取消收藏
	 */
	collectGoods(){

		let that = this,
			url = app.globalData.host + '/mobile/collect/save',
			obj = {
				petId: this.data.petId,
				token: app.globalData.token
			},
			msg = '加入成功'

		if (this.data.goodsData.isCollect == 1){
			url = app.globalData.host + '/mobile/collect/del'
			msg = '取消成功'
		}
		
		app.request('post', url, obj, function(res){
			if(res.data.code === '200'){
				
				wx.showToast({
					title: msg,
				})
				that.pageData();
			}
		})
	},

	/**
	 * 页面数据
	 */
	pageData(){
		
		let that = this,
			url = app.globalData.host + '/mobile/pet/findById',
			obj = {
				petId: this.data.petId,
				token: app.globalData.token
			}
		
		app.request('post', url, obj, function(res){
			if(res.data.code === '200'){
				let obj = res.data.result//vaccineInfos
				obj.vaccineInfos.forEach(v=>{
					v.createTime = v.createTime.slice(0, 4) + '-' + v.createTime.slice(4,6)
						+ '-' + v.createTime.slice(6,8)
				})
				that.setData({
					goodsData: obj
				})
			}
		})
	},

	/**
	 * 立即购买
	 */
	buyGoods(){
		wx.navigateTo({
			url: '../subOrder/subOrder?id=' + this.data.petId,
		})
	},

	/**
	 * 推荐人
	 */
	recommend(){
		wx.navigateTo({
			url: '../merchant/merchant',
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})