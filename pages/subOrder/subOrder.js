// pages/subOrder/subOrder.js
let app = getApp();
Page({
	/**
	 * 页面的初始数据
	 */
	data: {
		//地址信息
		addr:{},

		//商品信息
		goodsData:{
			price: 0
		}
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		this.setData({
			petId: opt.id
		})

		this.pageData();
	},

    /**
     * 提交订单
     */
    submitOrder(){

		if (!this.data.addr.phoneNum) {
			wx.showModal({
				title: '提示',
				content: '请完善收货地址',
				success(res) {
					setTimeout(function () {
						wx.navigateTo({
							url: '../infomation/infomation',
						})
					}, 1000)

				}
			})
			return;
		}

		// if (this.data.addr.cityCode != app.globalData.cityCode){
		// 	wx.showModal({
		// 		title: '提示',
		// 		content: '您的地址不支持配送，请填写当前城市地址',
		// 		success(res) {}
		// 	})
		// 	return;
		// }

        let that = this,
            url = app.globalData.host + '/mobile/order/submitOrder',
            obj = {
                petId: this.data.petId
            }
        app.request('post', url, obj, function (res) {
            if (res.data.code === '200') {
				that.pay(res.data.result)
            }
        })
    },

	/**
	 * 支付按钮
	 */
	payBtn(){
		
		if (!this.data.addr.phoneNum){
			wx.showModal({
				title: '提示',
				content: '请完善收货地址',
				success(res) {
					setTimeout(function () {
						wx.navigateTo({
							url: '../infomation/infomation',
						})
					}, 1000)

				}
			})
			return;
		}

		let that = this,
			url = app.globalData.host + '/appletPay',
			obj = {
				openId: app.globalData.openid
			}
		app.request('post', url, obj, function(res){
			if(res.data.code === '200'){
				that.pay(res.data.result)
			}
		})
	},

	/**
	 * 支付
	 */
	pay(o){
		let that = this;
		wx.requestPayment({
			timeStamp: o.timeStamp,
			nonceStr: o.nonceStr,
			package: o.package,
			signType: 'MD5',
			paySign: o.paySignStr,
			success(res) { 
				console.log(res)
				if (res.errMsg === "requestPayment:ok"){
					wx.showModal({
						title: '提示',
						content: '支付成功！',
						success(res) {
							setTimeout(function(){
								// wx.navigateBack(-1)
								wx.redirectTo({
									url: '../index/index',
								})
							},1000)
							
						}
					})
				}
			},
			fail(res) { }
		})
	},

	/**
	 * 页面数据
	 */
	pageData() {

		let that = this,
			url = app.globalData.host + '/mobile/pet/findById',
			obj = {
				petId: this.data.petId,
				token: app.globalData.token
			}

		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					goodsData: res.data.result
				})
			}
		})
	},

	/**
	 * 地址信息数据
	 */
	addressData(data) {
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = data || {}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					addr: res.data.result
				})
			}
		})
	},

	/**
	 * 修改地址信息
	 */
	editAddr(){
		wx.navigateTo({
			url: '../infomation/infomation',
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

		this.addressData();//地址信息

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})