// pages/wallet/wallet.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		pageNumber: 1,
		list: [],
		top: {
			earnings: {},
			withdraw: {}
		},
		earnings: 0,
		withdraw: 0
		
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {

		//this.getList()

	},

	/**
	 *  提现
	 */
	withdraw(){
		wx.navigateTo({
			url: '../withdraw/withdraw',
		})
	},


	topList(){
		let that = this,
			url = app.globalData.host + '/mobile/moneyRecord/findCountByMap',
			obj = {
				
			}
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {
				
				that.setData({
					top: res.data.result,
					earnings: (res.data.result.earnings.sumNum / 100).toFixed(2),
					withdraw: (res.data.result.withdraw.sumNum / 100).toFixed(2),
				})

			}
		})
	},

	/**
	 * 收益记录
	 */
	getList(){
		let that = this,
			url = app.globalData.host + '/mobile/moneyRecord/findByPage',
			obj = {
				type: 3,
				current: this.data.pageNumber,
			}
		app.request('post', url, obj, res=>{
			if(res.data.code === '200'){
				let list = res.data.result.records

				if(list.length == 0){
					wx.showToast({
						title: '没有更多数据了',
					})
					that.setData({
						pageNumber: that.data.pageNumber - 1
					})
					return;
				}

				list.forEach(v=>{
					v.time = v.createTime.slice(0,4) + '-' +
						v.createTime.slice(4, 6) + '-' +
						v.createTime.slice(6,8) + '' +
						v.createTime.slice(8, 10) + ':' +
						v.createTime.slice(10, 12) + ':' +
						v.createTime.slice(12, 14) 
				})

				that.setData({
					list: that.data.list.concat(list)
				})

			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.setData({
			pageNumber: 1,
			list: []
		})
		this.topList();
		this.getList();
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber + 1
		})
		this.getList()
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})