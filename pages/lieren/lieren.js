// pages/lieren/lieren.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		nav: '1',
		pageNumber: 1,

		list: [],

		info: {},
		total: 0,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		// 小程序二维码进入
		if (opt.scene) {
			let scene = decodeURIComponent(opt.scene);
			//&是我们定义的参数链接方式
			let userId = scene.split("&")[0];
			let petId = scene.split('&')[1];
			this.setData({
				petId: petId,
				shareUserId: userId,
				share: true,
				//hunters: app.globalData.hunters
			})
			
		}

		this.hasToken();
	},

	hasToken() {
		let that = this;

		if (app.globalData.token) {

			this.getList();
			this.getInfo();

		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	/**
	 *  注册二维码
	 */
	registerCode(){
		wx.navigateTo({
			url: '../hunterCode/hunterCode',
		})
	},

	/**
	 * 空间二维码
	 */
	roomCodse(e){
		wx.navigateTo({
			url: '../qrcode/qrcode',
		})
	},

	/**
	 * 分享二维码
	 */
	shareCode(e){

		wx.navigateTo({
			url: '../goodsCode/goodsCode?id=' + e.currentTarget.dataset.id + '&img=' + e.currentTarget.dataset.img + '&name=' + e.currentTarget.dataset.name + '&price=' + e.currentTarget.dataset.price / 100,
		})
	},

	/**
	 *  佣金管理
	 */
	wallet(){
		wx.navigateTo({
			url: '../wallet/wallet',
		})
	},

	/**
	*  获取商品列表
	*/
	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/collect/findByPage',
			obj = {
				size: 50,
				current: this.data.pageNumber,
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: that.data.list.concat(res.data.result.records),
					total: res.data.result.total
				})
			}
		})
	},

	/**
	 * 我的猎人
	 */
	getLower() {
		let that = this,
			url = app.globalData.host + '/mobile/user/findLower',
			obj = {
				size: 100,
				current: this.data.pageNumber,
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: that.data.list.concat(res.data.result.records)
				})
			}
		})
	},

	/**
	 * 我的信息
	 */
	getInfo() {
		let that = this,
			url = app.globalData.host + '/mobile/user/findById',
			obj = {
				// size: 50,
				// current: this.data.pageNumber,
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					info: res.data.result
				})
			}
		})
	},

	/**
	 * 导航
	 */
	navClick(e){
		this.setData({
			nav: e.currentTarget.dataset.id,
			pageNumber: 1,
			list: []
		})

		if(this.data.nav == 1){
			this.getList();
		}else{
			this.getLower();
		}
	},

	toDetail(e){
		wx.navigateTo({
			url: '../detail1/detail1?id=' + e.currentTarget.dataset.id,
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {
		
	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		this.setData({
			pageNumber: this.data.pageNumber + 1
		})
		if (this.data.nav == 1) {
			this.getList()
		} else {
			this.getLower()
		}
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})