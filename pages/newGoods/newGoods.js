// pages/newGoods/newGoods.js
let app =  getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		isRecommend: '',
		pageNumber: 1,
		list: [],
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (opt) {
		console.log(opt)
		console.log(app.globalData.hunters)
		if (opt.id == 'boutique'){
			this.setData({
				isRecommend: '1',
			})
		}
		this.setData({
			hunters: app.globalData.hunters
		})
		this.getList()
	},

	/**
	 *  获取商品列表
	 */
	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/pet/findByPage',
			obj = {
				size: 50,
				current: this.data.pageNumber,
				isRecommend: this.data.isRecommend
			}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				
				if (res.data.result.records.length == 0){
					wx.showToast({
						title: '没有更多数据了',
					})
					that.setData({
						pageNumber: that.data.pageNumber == 1 ? 1 : that.data.pageNumber - 1
					})
					return;
				}

				that.setData({
					list: that.data.list.concat(res.data.result.records) 
				})
			}
		})
	},

	/**
	 * 商品详情
	 */
	toDetail(e){
		wx.navigateTo({
			url: '../detail1/detail1?id=' + e.currentTarget.dataset.id,
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {
		console.log('up')
		this.setData({
			pageNumber: this.data.pageNumber + 1
		})
		this.getList()
	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})