// pages/hunterCode/hunterCode.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {

	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this;
		wx.getImageInfo({
			src: app.globalData.host + '/getwxacodeunlimit?page=pages/huntsman/huntsman&petId=&userId=' + app.globalData.userId,    //请求的网络图片路径
			success: function (res) {
				console.log(res)
				console.log(res.path)
				//请求成功后将会生成一个本地路径即res.path,然后将该路径缓存到storageKeyUrl关键字中
				that.setData({
					codesrc: res.path
				})

				that.drawCanvas()

			}
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {
		//this.drawCanvas();
	},

	drawCanvas() {
		let that = this
		const ctx = wx.createCanvasContext('firstCanvas')

		ctx.drawImage('../../images/lieren_code.png', 0, 0, 320, 500)

		ctx.drawImage(that.data.codesrc, 85, 110, 150, 150)

		ctx.draw(true, function () {
			that.dramimg(ctx)
		})
	},

	/**
	 *  导出图片
	 */
	dramimg(ctx) {
		let that = this
		console.log(222)
		wx.canvasToTempFilePath({
			x: 0,
			y: 0,
			width: 320,
			height: 500,
			destWidth: 640,
			destHeight: 880,
			canvasId: 'firstCanvas',
			success(res) {
				console.log(res.tempFilePath)
				that.setData({
					url: res.tempFilePath
				})

				wx.previewImage({
				    current: res.tempFilePath, // 当前显示图片的http链接
				    urls: [res.tempFilePath], // 需要预览的图片http链接列表
				    complete: str=>{
				        console.log(str)
				    }
				})
			}
		})
	},

	/**
	 *  预览图片
	 */
	preImg() {
		wx.previewImage({
			current: this.data.url, // 当前显示图片的http链接
			urls: [this.data.url], // 需要预览的图片http链接列表
			complete: str => {
				console.log(str)
			}
		})
	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})