// pages/huntsman/huntsman.js
let app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		checked:false,
		list: {},

		hasInfo: true, 
		userInfo: null,

		paySt: true
	},

	/**
	 * 生命周期函数--监听页面加载 
	 */
	onLoad: function (opt) {

		if (opt.scene) {
			let scene = decodeURIComponent(opt.scene);
			//&是我们定义的参数链接方式
			let userId = scene.split("&")[0];
			let petId = scene.split('&')[1];
			this.setData({
				petId: petId,
				shareUserId: userId,
			})

		}

		this.hasToken();
	},

	hasToken() {
		let that = this;

		if (app.globalData.token) {

			if (app.globalData.hunters){
				wx.redirectTo({
					url: '../lieren/lieren',
				})
				return;
			}

			app.location()

			this.getList();

			if (this.data.shareUserId) {
				this.bindRecommend();
			}
			console.log(app.globalData.userInfo)

			if (!app.globalData.userInfo) {
				this.setData({
					hasInfo: false
				})
			}


		} else {
			setTimeout(() => {
				that.hasToken();
			}, 1000)
		}
	},

	getUserInfo(e) {
		let that = this;
		console.log(e)
		if (e.detail.userInfo) {
			app.globalData.userInfo = e.detail.userInfo
			app.saveWXInfo(e.detail.userInfo)
			this.setData({
				userInfo: e.detail.userInfo,
				hasInfo: true
			})
		}

	},

	/**
	 *  分享二维码进入绑定推荐关系
	 */
	bindRecommend() {
		let that = this,
			url = app.globalData.host + '/mobile/user/updateById',
			obj = {
				recommendUserId: that.data.shareUserId || 1,
				// userId: app.globalData.userId || 2 
			}

		if (!app.globalData.cityCode) {
			setTimeout(function () {
				that.bindRecommend();
			}, 1000)
			return;
		}
		
		app.request('post', url, obj, res => {
			if (res.data.code === '200') {

			}
		})
	},

	getList() {
		let that = this,
			url = app.globalData.host + '/mobile/globalConfig/findById',
			obj = {}
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.setData({
					list: res.data.result
				})
			}
		})
	},

	/**
	 * 注册赏金猎人
	 */
	register(){
		if (!this.data.paySt){
			wx.showToast({
				title: '正在处理',
				icon: 'none'
			})
			return;
		}
		
		let that = this,
			url = app.globalData.host + '/mobile/bountyHunters/payRequest',
			obj = {
				
			}
		that.setData({
			paySt: false
		})
		app.request('post', url, obj, function (res) {
			if (res.data.code === '200') {
				that.pay(res.data.result)
			}
			
		})


		
	},

	/**


	/**
	 * 支付
	 */
	pay(o) {
		this.setData({
			paySt: true
		})
		let that = this;
		wx.requestPayment({
			timeStamp: o.timeStamp,
			nonceStr: o.nonceStr,
			package: o.package,
			signType: 'MD5',
			paySign: o.paySignStr,
			success(res) {
				console.log(res)
				if (res.errMsg === "requestPayment:ok") {
					wx.showModal({
						title: '提示',
						content: '支付成功！请填写资料完成注册',
						success(res) {
							setTimeout(function () {
								wx.navigateTo({
									url: '../register/register?money=' + that.data.list.huntersApplyPrice,
								})
							}, 1000)

						}
					})
				}
			},
			fail(res) { }
		})
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})